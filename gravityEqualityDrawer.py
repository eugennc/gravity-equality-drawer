#!/usr/bin/python3
"""
This program draws an image, displaying the relative gravitational influences of two bodies of different masses in 2D space (but 3d symmetric under bodies axis rotation).
"""
import os, sys
import time
import math

import tkinter
import base64

maxProp = 1

def FG(mass, rm, r):
    '''mass, the position vector of the mass, the position vector of the point we compute F_G for '''
    #we assume k_G = 1 i.e. natural units in this context, and the current mass to be 1 as well. We can also say we compute the acceleration on a constant third body.
    dx = r[0] - rm[0]
    dy = r[1] - rm[1]
    dr2 = dx**2 + dy**2
    f = 0
    if dr2 != 0:
        f = mass / dr2
    return f

def create3DHolor(dimensions):
    h = list()
    for ii in range(dimensions[0]):
        h.append(list())
        for jj in range(dimensions[1]):
            h[ii].append(list())
            for _ in range(dimensions[2]):
                h[ii][jj].append(0)
    return(h)
                

def drawForceProp(x, y, prop, imgData, maxProp = maxProp):
    minCol = 255
    maxCol = 0
    if prop > maxProp:
        return()
    propMul = prop / maxProp
    col = minCol + (maxCol - minCol) * propMul
    col = hex(int(col)).lstrip('0').lstrip('x')
    if len(col) < 2:
        col = '0' + col
    fullCol = '#' + 3 * col
    imgData.put(fullCol, (x, y))

def main():
    width = 1000
    height = 1000
    zoomOut = 6
    minBodySize = 4

    numberOfMasses = 2

    masses = []
    forces = create3DHolor( (width, height, numberOfMasses) )

    #create the GUI
    root = tkinter.Tk()
    canvas = tkinter.Canvas(root, confine = False, width = width, height = height)
    canvas.pack()
    pic = tkinter.PhotoImage(width = width, height = height)
    canvas.create_rectangle(0, 0, width, height, outline = 'white', fill = 'white')
    #draw an ortogonal grid
    for ii in range(width // 10 + 1):
        canvas.create_line(10 * ii, 0, 10 * ii, height, fill = '#505050')
    for ii in range(height // 10 + 1):
        canvas.create_line(0, 10 * ii, width, 10 * ii, fill = '#505050')

    for index in range(numberOfMasses):
        rx = width / (2 * zoomOut)
        ry = height / (2 * zoomOut)
        px = zoomOut * rx + rx * math.cos(2 * math.pi * index / numberOfMasses)
        py = zoomOut * ry + ry * math.sin(2 * math.pi * index / numberOfMasses)
        masses.append([index + 1, [px, py]])

    #we can define the mass values of the objects
    if len(sys.argv) == 3:
        for ii, mass in enumerate(sys.argv[1:]):
            masses[ii][0] = float(mass)

#    for x in range(width):
#        for y in range(height):
#            for ii, mass in enumerate(masses):
#                f = FG(mass[0], mass[1], [x, y])
#                #print('m = ', mass[0], 'rm = ', mass[1], 'r = ', [x, y], 'f = ', f)
#                forces[x][y][ii] = f
#                firstModulus = forces[x][y][0]
#                epsilon = firstModulus / 100
#                low = firstModulus - epsilon
#                high = firstModulus + epsilon
#                
#                modulusDiff = 0
#                equalForces = True
#                for modulus in forces[x][y][1:]:
#                    if modulus > high or modulus < low:
#                        equalForces = False
#                        break
#                if equalForces:
#                    drawForceProp(x, y, 0.1, canvas)

    for x in range(width):
        for y in range(height):
            for ii, mass in enumerate(masses):
                f = FG(mass[0], mass[1], [x, y])
                #print('m = ', mass[0], 'rm = ', mass[1], 'r = ', [x, y], 'f = ', f)
                forces[x][y][ii] = f
                firstModulus = forces[x][y][0]
                epsilon = firstModulus / 100
                modulusDiff = 0
                for modulus in forces[x][y][1:]:
                    diff = abs(modulus - firstModulus)
                    fD = False
                    sD = False
                    if firstModulus != 0:
                        firstDiv = diff / firstModulus
                        fD = True
                    if modulus != 0:
                        secondDiv = diff / modulus
                        sD = True

                    if fD and sD:
                        if modulusDiff < firstDiv:
                            modulusDiff = firstDiv
                        if modulusDiff < secondDiv:
                            modulusDiff = secondDiv
                    elif fD == True:
                        if modulusDiff < firstDiv:
                            modulusDiff = firstDiv
                    elif sD == True:
                        if modulusDiff < secondDiv:
                            modulusDIff = secondDif
 
                drawForceProp(x, y, modulusDiff, pic)
                
    canvas.create_image(0, 0, image = pic, anchor = tkinter.NW)

    for body in masses:
        colour = 'orange'
        mass = body[0]
        size = minBodySize
        if(mass > 0):
            size = size + math.log(mass)
        else:
            size = size + math.log(abs(mass))
            colour = 'purple'
        px, py = body[1]
        print('x:', px, 'y:', py, 'mass:', mass)
        canvas.create_oval(px - size, py - size, px + size, py + size, fill = colour, outline = '')

    #print(forces)
    root.mainloop()

main()
